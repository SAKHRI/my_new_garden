<?php

namespace App\EventSubscriber;

use App\Repository\CustomEventRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    private $customeventRepository;
    private $router;

    public function __construct(
        CustomEventRepository $customeventRepository,
        UrlGeneratorInterface $router,
        TokenStorageInterface $tokenStorage
    ) {
        $this->customeventRepository = $customeventRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

        // Modify the query to fit to your entity and needs
        // Change booking.beginAt by your start date property
        $customevents = $this->customeventRepository
            ->createQueryBuilder('customevent')
            ->where('customevent.beginAt BETWEEN :start and :end OR customevent.endAt BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            // ->andWhere('customevent.author = :user')
            // ->setParameter('user', $this->tokenStorage->getToken()->getUser())
            ->getQuery()
            ->getResult()
        ;
       


        foreach ($customevents as $customevent) {
            // this create the events with your data (here booking data) to fill calendar
            $customEvent = new Event(
                $customevent->getTitle(),
                $customevent->getBeginAt(),
                $customevent->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $customEvent->setOptions([
                'backgroundColor' => 'blue',
                'borderColor' => 'red',
                'textColor:' => 'black',
            ]);
            $customEvent->addOption(
                'url',
                $this->router->generate('garden_show', [
                    'id' => $customevent->getGarden()->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($customEvent);
        }
    }
}