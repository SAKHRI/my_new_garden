<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\CustomEvent;
use App\Entity\Garden;
use App\Entity\Task;
use App\Entity\Todolist;
use App\Entity\Upload;
use App\Form\CommentType;
use App\Form\CustomEventType;
use App\Form\GardenType;
use App\Form\TaskType;
use App\Form\TodolistType;
use App\Form\UploadType;
use App\Repository\GardenRepository;
use App\Service\FileUploader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/garden")
 */
class GardenController extends AbstractController
{
    /**
     * @Route("/", name="garden_index", methods={"GET"})
     */
    public function index(GardenRepository $gardenRepository): Response
    {
        return $this->render('garden/index.html.twig', [
            'gardens' => $gardenRepository->findBy(array(), array('id' => 'desc')),
        ]);    
    }

    /**
     * @Route("/new", name="garden_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $garden = new Garden();
        $form = $this->createForm(GardenType::class, $garden);
        $form->handleRequest($request);

        $todolist = new Todolist();

        if ($form->isSubmitted() && $form->isValid()) {

            $garden->setCreatedAt(new \DateTime());
            $garden = $form->getData();
            $picture = $form->get('picture')->getData();


            if ($picture) {
                $pictureFileName = $fileUploader->upload($picture);
                $garden->setPicture($pictureFileName);
            }



            // $garden = $form->getData();
            // $picture = $form->get('picture')->getData();

            // $pictureName = md5(uniqid()) . '.' . $picture->guessExtension();

            // $picture->move($this->getParameter('images_directory'), $pictureName);

            // $garden->setPicture($pictureName);

            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            $user = $this->getUser();
            $garden->setAuthor($user);

            $todolist->setName("La Todolist du Jardin");
            $todolist->setGarden($garden);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($garden);
            $entityManager->persist($todolist);
            $entityManager->flush();

            return $this->redirectToRoute('garden_index');
        }

        return $this->render('garden/new.html.twig', [
            'garden' => $garden,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="garden_show", methods={"GET", "POST", "PUT"})
     */
    public function show(Garden $garden, Request $request, FileUploader $fileUploader): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            $user = $this->getUser();
            $comment->setAuthor($user);
            $comment->setCreatedAt(new \DateTime());

            $comment->setGarden($garden);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute("garden_show", ['id' => $garden->getId()]);
        }



        $task = new Task();
        $form1 = $this->createForm(TaskType::class, $task);
        $form1->handleRequest($request);


        if ($form1->isSubmitted() && $form1->isValid()) {

            $task->setTodolist($garden->getTodo());


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute("garden_show", ['id' => $garden->getId()]);
        }


        $upload = new Upload();
        $form2 = $this->createForm(UploadType::class, $upload);
        $form2->handleRequest($request);

        if ($form2->isSubmitted() && $form2->isValid()) {
            
            $file = $form2->get('file')->getData();

            if ($file) {
                $originalName = $fileUploader->originalName($file);
                $upload->setTitle($originalName);

                $fileName = $fileUploader->upload($file);
                $upload->setFile($fileName);
            }

            $upload->setGarden($garden);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($upload);
            $entityManager->flush();

            return $this->redirectToRoute("garden_show", ['id' => $garden->getId()]);
        }

        $customevent = new CustomEvent();
        $form3 = $this->createForm(CustomEventType::class, $customevent);
        $form3->handleRequest($request);


        if ($form3->isSubmitted() && $form3->isValid()) {

            $customevent->setGarden($garden);
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
            $user = $this->getUser();
            $customevent->setAuthor($user);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customevent);
            $entityManager->flush();

            return $this->redirectToRoute("garden_show", ['id' => $garden->getId()]);
        }

        return $this->render('garden/show.html.twig', [
            'garden' => $garden,
            'commentForm' => $form->createView(),
            'taskForm' => $form1->createView(),
            'uploadForm' => $form2->createView(),
            'customeventForm' => $form3->createView()

        ]);
    }

    /**
     * @Route("/{id}/edit", name="garden_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Garden $garden, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(GardenType::class, $garden);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $garden = $form->getData();
            $picture = $form->get('picture')->getData();

            if ($picture) {
                $pictureFileName = $fileUploader->upload($picture);
                $garden->setPicture($pictureFileName);
            }


            // $pictureName = md5(uniqid()) . '.' . $picture->guessExtension();

            // $picture->move($this->getParameter('images_directory'), $pictureName);

            // $garden->setPicture($pictureName);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('garden_index');
        }

        return $this->render('garden/edit.html.twig', [
            'garden' => $garden,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="garden_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Garden $garden): Response
    {
        if ($this->isCsrfTokenValid('delete' . $garden->getId(), $request->request->get('_token'))) {


            $picture = $garden->getPicture();
            $filesystem = new Filesystem();
            $filesystem->remove($this->getParameter('images_directory') . $picture);


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($garden);
            $entityManager->flush();
        }

        return $this->redirectToRoute('garden_index');
    }
}
