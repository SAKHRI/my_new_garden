<?php

namespace App\Controller;

use App\Entity\CustomEvent;
use App\Form\CustomEventType;
use App\Repository\CustomEventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/event")
 */
class CustomEventController extends AbstractController
{
    /**
     * @Route("/", name="custom_event_index", methods={"GET"})
     */
    public function index(CustomEventRepository $customEventRepository): Response
    {
        return $this->render('custom_event/index.html.twig', [
            'custom_events' => $customEventRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="custom_event_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $customEvent = new CustomEvent();
        $form = $this->createForm(CustomEventType::class, $customEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customEvent);
            $entityManager->flush();

            return $this->redirectToRoute('custom_event_index');
        }

        return $this->render('custom_event/new.html.twig', [
            'custom_event' => $customEvent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="custom_event_show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function show(CustomEvent $customEvent): Response
    {
        return $this->render('custom_event/show.html.twig', [
            'custom_event' => $customEvent,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="custom_event_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CustomEvent $customEvent): Response
    {
        $form = $this->createForm(CustomEventType::class, $customEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('garden_show', ["id" => $customEvent->getGarden()->getId()]);
            // return $this->redirectToRoute('custom_event_index');
        }

        return $this->render('custom_event/edit.html.twig', [
            'custom_event' => $customEvent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="custom_event_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CustomEvent $customEvent): Response
    {
        if ($this->isCsrfTokenValid('delete'.$customEvent->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($customEvent);
            $entityManager->flush();
        }

        return $this->redirectToRoute('garden_show', ["id" => $customEvent->getGarden()->getId()]);

        // return $this->redirectToRoute('custom_event_index');
    }

    /**
     * @Route("/calendar", name="custom_event_calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        return $this->render('custom_event/calendar.html.twig');
    }
}
