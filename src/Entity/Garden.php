<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GardenRepository")
 */
class Garden
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $area;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="gardens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="gardens")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="garden", orphanRemoval=true, cascade={"all"})
     */
    private $comments;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Todolist", inversedBy="garden", cascade={"persist", "remove"})

     */
    private $todo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Upload", mappedBy="garden", orphanRemoval=true)
     */
    private $uploads;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomEvent", mappedBy="garden", orphanRemoval=true)
     */
    private $customEvents;

    public function __construct()
    {
        $this->uploads = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->customEvents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getArea(): ?int
    {
        return $this->area;
    }

    public function setArea(?int $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setGarden($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getGarden() === $this) {
                $comment->setGarden(null);
            }
        }

        return $this;
    }

    public function getTodo(): ?Todolist
    {
        return $this->todo;
    }

    public function setTodo(?Todolist $todo): self
    {
        $this->todo = $todo;

        return $this;
    }

    /**
     * @return Collection|Upload[]
     */
    public function getUploads(): Collection
    {
        return $this->uploads;
    }

    public function addUpload(Upload $upload): self
    {
        if (!$this->uploads->contains($upload)) {
            $this->uploads[] = $upload;
            $upload->setGarden($this);
        }

        return $this;
    }

    public function removeUpload(Upload $upload): self
    {
        if ($this->uploads->contains($upload)) {
            $this->uploads->removeElement($upload);
            // set the owning side to null (unless already changed)
            if ($upload->getGarden() === $this) {
                $upload->setGarden(null);
            }
        }

        return $this;
    }

        // ----------------------

        public function __toString()
        {
            // to show the name of the Category in the select
            return $this->name;
            // to show the id of the Category in the select
            // return $this->id;
        }

        /**
         * @return Collection|CustomEvent[]
         */
        public function getCustomEvents(): Collection
        {
            return $this->customEvents;
        }

        public function addCustomEvent(CustomEvent $customEvent): self
        {
            if (!$this->customEvents->contains($customEvent)) {
                $this->customEvents[] = $customEvent;
                $customEvent->setGarden($this);
            }

            return $this;
        }

        public function removeCustomEvent(CustomEvent $customEvent): self
        {
            if ($this->customEvents->contains($customEvent)) {
                $this->customEvents->removeElement($customEvent);
                // set the owning side to null (unless already changed)
                if ($customEvent->getGarden() === $this) {
                    $customEvent->setGarden(null);
                }
            }

            return $this;
        }
    
        // -----------------------
}
