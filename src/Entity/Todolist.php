<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TodolistRepository")
 */
class Todolist
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="todolist", orphanRemoval=true)
     */
    private $tasks;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Garden", mappedBy="todo", cascade={"persist", "remove"})
     */
    private $garden;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setTodolist($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            // set the owning side to null (unless already changed)
            if ($task->getTodolist() === $this) {
                $task->setTodolist(null);
            }
        }

        return $this;
    }

    public function __toString()
        {
            // to show the name of the Category in the select
            return $this->name;
            // to show the id of the Category in the select
            // return $this->id;
        }

    public function getGarden(): ?Garden
    {
        return $this->garden;
    }

    public function setGarden(?Garden $garden): self
    {
        $this->garden = $garden;

        // set (or unset) the owning side of the relation if necessary
        $newTodo = $garden === null ? null : $this;
        if ($newTodo !== $garden->getTodo()) {
            $garden->setTodo($newTodo);
        }

        return $this;
    }
}
