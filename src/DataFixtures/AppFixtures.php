<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use Faker\Factory;
use App\Entity\Garden;
use App\Entity\Task;
use App\Entity\Todolist;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    public function load(ObjectManager $manager)
    {

        // On configure dans quelles langues nous voulons nos données
        $faker = Factory::create('fr_FR');



        for ($u = 0; $u < 5; $u++) {
            $user = new User();
            $user->setEmail(sprintf('userdemo%d@example.com', $u));
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'userdemo'
            ));
            $user->setRoles(['ROLE_USER']);
            $user->setName($faker->firstName);
            $user->setFirstName($faker->lastName);
            $user->setBirthdate(new \DateTime());

            $manager->persist($user);


            for ($c = 0; $c < 1; $c++) {
                $category = new Category();
                $category->setName($faker->colorName);
                $manager->persist($category);


                for ($g = 0; $g < 3; $g++) {
                    $garden = new Garden();
                    $garden->setName($faker->company);
                    $garden->setPicture($faker->imageUrl(400, 400));
                    $garden->setContent($faker->text);
                    $garden->setCreatedAt(new \DateTime());
                    $garden->setCity($faker->city);
                    $garden->setAddress($faker->address);
                    $garden->setZipcode(mt_rand(69000, 70000));
                    $garden->setArea(mt_rand(20, 100));
                    $garden->setCategory($category);
                    $garden->setAuthor($user);

                    $manager->persist($garden);

                    for ($cm=0; $cm < 5; $cm++) { 
                        $comment = new Comment();
                        $comment->setContent("Mon commentaire $cm");
                        $comment->setCreatedAt(new \DateTime());
                        $comment->setGarden($garden);
                        $comment->setAuthor($user);

                        $manager->persist($comment);

                    }

                        $todolist = new Todolist();
                        $todolist->setName("La Todolist du jardin");
                        $todolist->setGarden($garden);

                        $manager->persist($todolist);

                        for ($t=0; $t < 5; $t++) { 
                            $task = new Task();
                            $task->setName("ma tache $t");
                            $task->setTodolist($todolist);
    
                            $manager->persist($task);
    
                        }
                    
                }
            }
        }


        $manager->flush();
    }
}


// php bin/console doctrine:fixtures:load
